#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define MAXLINE 10000
#define SERV_TCP_PORT 10110
#define MAX_CMDNUMBER 4000
#define PATHSIZE 1024
#define BUFFSIZE 8192

fd_set master,readfd;
typedef struct usertable{
	int valid;
	int sockfd;
	char name[21];
	char ip_port[22];
	char path[1024];
	int pipetable[1000][2];
	int pipeto[30];
	int pipefd[30];
}Usertable;
Usertable clitable[30];

int err_dump(char* p){
	perror(p);
	exit(1);
}

int findme(int sockfd){
	for(int i=0;i<30;i++){
		if(clitable[i].valid==1&&clitable[i].sockfd==sockfd){
			return i;
		}
	}
	return -1;
}

void initenv(){
	char path[PATHSIZE];
	getcwd(path,PATHSIZE);
	strcat(path,"/res");
	chdir(path);
	chroot(path);
	for(int i=0;i<30;i++){
		clitable[i].valid=0;
		clitable[i].sockfd=0;
		strcpy(clitable[i].name,"(no name)");
		strcpy(clitable[i].path,"bin:.");
		memset(clitable[i].ip_port,0,22);
		for(int k=0;k<30;k++){
			clitable[i].pipeto[k]=0;
			clitable[i].pipefd[k]=0;
		}
		for(int j=0;j<1000;j++){
			clitable[i].pipetable[j][0]=0;
			clitable[i].pipetable[j][1]=0;
		}
	}
}

void broad_cast(int flag,int sockfd,int no,char *line){
	int me=findme(sockfd);
	char *buff=malloc(sizeof(char)*1024);
	if(flag==1){
		sprintf(buff,"*** User '(no name)' entered from %s. ***\n",clitable[me].ip_port);
	}
	if(flag==2){
		sprintf(buff,"*** User '%s' left. ***\n",clitable[me].name);
	}
	if(flag==3){
		sprintf(buff,"*** User from %s is named '%s'. ***\n",clitable[me].ip_port,clitable[me].name);
	}
	if(flag==6){
		sprintf(buff,"*** %s (#%d) just piped '%s' to %s (#%d) ***\n",clitable[me].name,me+1,\
						line,clitable[no].name,no+1);
	}
	if(flag==7){
		sprintf(buff,"*** %s (#%d) just received from %s (#%d) by '%s' ***\n",clitable[me].name,me+1,\
						clitable[no].name,no+1,line);
	}
	for(int i=0;i<30;i++){
		if(clitable[i].valid==1){
			write(clitable[i].sockfd,buff,strlen(buff));
		}
	}
	free(buff);
}

void create_user(struct sockaddr_in cli_addr,int sockfd){
	// char name[20]={0};
	for(int i=0;i<30;i++){
		if(clitable[i].valid==0){
			clitable[i].valid=1;
			clitable[i].sockfd=sockfd;
			// inet_ntop(AF_INET,&cli_addr.sin_addr,shm_user[i].ip_port,16);
			// strcat(shm_user[i].ip_port,"/");
			// sprintf(name,"%u",cli_addr.sin_port);
			// strcat(shm_user[i].ip_port,name);
			// strcpy(shm_event->message,shm_user[i].ip_port);
			strcpy(clitable[i].ip_port,"CGILAB/511");
			break;
		}
	}
	broad_cast(1,sockfd,0,0);
}

void delete_user(int sockfd){
	int i=findme(sockfd);
	broad_cast(2,sockfd,0,0);
	for(int j=0;j<30;j++){
		if(clitable[j].pipeto[i]==1){
			clitable[j].pipeto[i]=0;
			close(clitable[j].pipefd[i]);
		}
	}
	clitable[i].valid=0;
	memset(clitable[i].ip_port,0,22);
	strcpy(clitable[i].name,"(no name)");
}

void profix(int sockfd){
	char *pro = "% ";
	write(sockfd,pro,2);
}

//	readline also cut blank

int readline(int fd,char *ptr, int maxlen){
	int n, rc;
	char c;
	for(n=1;n<maxlen; n++){
		if ((rc=read(fd, &c, 1))==1){
			if (c == '\r')
				continue;
			if (c == '\n')
				break;
			*ptr++ = c;
		}else if(rc==0){
			if(n==1) return(0);
			else break;
		}else
			return(-1);
	}
	*ptr = '\0';
	return(n);
}

// cut the line

int cutline(char *line, char *cmd[], char *cut){
	if(!line) return 0;
	char *str = NULL;
	char *ptr = NULL;
	char *dup = strdup(line);

	str = strtok_r(dup, cut, &ptr);
	if (str == NULL){
		err_dump("error : client no input\n");
		free(dup);
		return -1;
	}
	int count =0;
	do{
		cmd[count]=strdup(str);
		str = strtok_r(NULL, cut, &ptr);
		count++;
	}while(str);
	cmd[count]=NULL;
	free(dup);
	return count;
}
// environment configure

void exit_cmd(int sockfd){
	delete_user(sockfd);
	FD_CLR(sockfd,&master);
	close(sockfd);
}

void printenv(int sockfd,char *cmd[]){
	if (cmd[1] != NULL){
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		sprintf(text,"%s=%s\n",cmd[1],getenv(cmd[1]));
		write(sockfd,text,strlen(text));
		free(text);
	}
}

void setenv_cmd(int sockfd, char *cmd[]){
	int me=findme(sockfd);
	strcpy(clitable[me].path,cmd[2]);
}

void tableclock(int sockfd){
	int me=findme(sockfd);
	for(int i=0;i<1000;i++){
		if(clitable[me].pipetable[i][0]>0){
			if(clitable[me].pipetable[i][0]==1){
				clitable[me].pipetable[i][0]=-1;
				dup2(clitable[me].pipetable[i][1],0);
			}
			else clitable[me].pipetable[i][0]--;
		}
	}
}

void pluspipe(int sockfd){
	int me=findme(sockfd);
	for(int i=0;i<1000;i++){
		if(clitable[me].pipetable[i][0]>0){
				clitable[me].pipetable[i][0]++;
			}
			else if(clitable[me].pipetable[i][0]==-1) clitable[me].pipetable[i][0]=1;
		}
}

void closepipe(int sockfd){
	int me=findme(sockfd);
	if (me==-1) return;
	for(int i=0;i<1000;i++){
		if(clitable[me].pipetable[i][0]==-1){
			clitable[me].pipetable[i][0]=0;
			close(clitable[me].pipetable[i][1]);
		}
	}
}

void createpipe(int pn,int fd0,int fd1,int sockfd){
	int me=findme(sockfd);
	int insert=-1;
	for(int i=0;i<1000;i++){
		if(clitable[me].pipetable[i][0]==0){
			clitable[me].pipetable[i][1]=fd0;
			clitable[me].pipetable[i][0]=pn;
			insert = i;
			break;
		}
	}
	if(insert==-1) err_dump("pipetable full");
	int len=0;
	char *buff;
	buff=malloc((BUFFSIZE+1)*sizeof(char));
	for(int i=0;i<1000;i++){
		if(i==insert) continue;
		else if(clitable[me].pipetable[i][0]==pn){
			while((len=read(clitable[me].pipetable[i][1],buff,BUFFSIZE))){
			buff[len]=0;
			write(fd1,buff,strlen(buff));
			}
			close(clitable[me].pipetable[i][1]);
			clitable[me].pipetable[i][0]=0;
			break;
		}
	}
	free(buff);
}

// check pipe

int classify_X(char *cmd[],int count){
	if(strcmp(cmd[0],"yell")==0) return 3;
	if(strcmp(cmd[0],"tell")==0) return 3;
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return 1;
		if (cmd[i][0]=='<') return 5;
		if (cmd[i][0]=='>'&&cmd[i][1]!=0) return 4;
		if (cmd[i][0]=='>') return 2;
	}
	return 0;
}

int returnX(char *cmd[],int count){
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return i;
		if (cmd[i][0]=='>') return i;
		if (cmd[i][0]=='<') return i;
	}
	return 0;
}

int splitbyX(char* front[],char* cmd[],int pn,int cn){
  char* t[MAX_CMDNUMBER];
  for(int i=0;i<cn;i++){
    t[i]=strdup(cmd[i]);
  }
  for(int i=0;i<cn+1;i++){
    free(cmd[i]);
  }
  for(int i=0;i<pn;i++){
    front[i]=strdup(t[i]);
  }
  front[pn]=0;
  for(int i=pn+1;i<cn;i++){
    cmd[i-pn-1]=strdup(t[i]);
  }
  cmd[cn-pn-1]=0;
  for(int i=0;i<cn;i++){
    free(t[i]);
  }
  return cn-pn-1;
}

void execvpplus(int sockfd,char *cmd[]){
	char *text;
	text = malloc((sizeof(char))*(BUFFSIZE));
	if(execvp(cmd[0],cmd)==-1){
		sprintf(text,"Unknown command: [%s].\n",cmd[0]);
		write(sockfd,text,strlen(text));
		free(text);
		exit(1);
	}
	free(text);
	exit(0);
}

void freecmd(char *cmd[],int count){
	for(int i=0;i<count;i++)
	free(cmd[i]);
}

void who_cmd(int sockfd){
	char *buff;
	buff=malloc(100*sizeof(char));
	int t=findme(sockfd);
	sprintf(buff,"<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
	write(sockfd,buff,strlen(buff));
	for (int i=0;i<30;i++){
		if(clitable[i].valid==1){
			sprintf(buff,"%d\t%s\t%s",i+1,clitable[i].name,clitable[i].ip_port);
			if(i==t) strcat(buff,"\t<-me\n");
			else strcat(buff,"\n");
			write(sockfd,buff,strlen(buff));
		}
	}
	free(buff);
}

void name_cmd(char *cmd[],int sockfd){
	char *str=malloc(BUFFSIZE*sizeof(char));
	for(int i=0;i<30;i++){
		if(strcmp(clitable[i].name,cmd[1])==0){
			sprintf(str,"*** User '%s' already exists. ***\n",cmd[1]);
			write(sockfd,str,strlen(str));
			free(str);
			return;
		}
	}
	int i=findme(sockfd);
	strcpy(clitable[i].name,cmd[1]);
	free(str);
	broad_cast(3,sockfd,0,0);
}

void yell_cmd(char *line,int number,int sockfd){
	char *ptr = NULL;
	char *dup = strdup(line),buff[1024];
	int me = findme(sockfd);
	strtok_r(dup," ", &ptr);
	sprintf(buff,"*** %s yelled ***: %s\n",clitable[me].name,ptr);
	for(int i=0;i<30;i++){
		if(clitable[i].valid==1){
			write(clitable[i].sockfd,buff,strlen(buff));
		}
	}
	free(dup);
}

void tell_cmd(char *cmd[],char *line,int number,int sockfd){
	char *ptr = NULL;
	char *dup = strdup(line);
	strtok_r(dup," ", &ptr);
	strtok_r(NULL," ", &ptr);
	int tellto=atoi(cmd[1]),rtell=tellto-1;
	int me=findme(sockfd);
	char buff[1024];
	if(clitable[rtell].valid==1){
		sprintf(buff,"*** %s told you ***: %s\n",clitable[me].name,ptr);
		write(clitable[rtell].sockfd,buff,strlen(buff));
	}
	else{
		char *buff=malloc(BUFFSIZE*sizeof(char));
		sprintf(buff,"*** Error: user #%d does not exist yet. ***\n",tellto);
		write(sockfd,buff,strlen(buff));
		free(buff);
	}
	free(dup);
}

int sendpipe_cmd(char *line,char* front[],char* cmd[],int sockfd,int cmd_number){
	int tag,to,rto,childpid,fd[2],me;
	me=findme(sockfd);
	tag=returnX(cmd,cmd_number);
	to=atoi(&cmd[tag][1]);
	rto=to-1;
	if (clitable[rto].valid==0){
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: user #%d does not exist yet. ***\n",to);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	else if(clitable[me].pipeto[rto]==0){
		clitable[me].pipeto[rto]=1;
		cmd_number=splitbyX(front,cmd,tag,cmd_number);
		if(cmd[0]!=0&&cmd[0][0]=='<'){
			int from,rfrom,fe[2];
			from=atoi(&cmd[0][1]);
			rfrom=from-1;
			clitable[rfrom].pipeto[me]=0;
			broad_cast(7,sockfd,rfrom,line);
			pipe(fe);
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(clitable[rfrom].pipefd[me],0);
				dup2(fe[1],1);
				dup2(sockfd,2);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(fe[1]);
			close(clitable[rfrom].pipefd[me]);
			clitable[me].pipefd[rto]=fe[0];
			freecmd(front,tag+1);
		}
		else{
			pipe(fd);
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(fd[1],1);
				dup2(sockfd,2);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(fd[1]);
			clitable[me].pipefd[rto]=fd[0];
			freecmd(front,tag+1);
		}
		broad_cast(6,sockfd,rto,line);
	}
	else{
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: the pipe #%d->#%d already exists. ***\n",me+1,to);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	freecmd(cmd,cmd_number+1);
	return 0;
}

int readpipe_cmd(char *line,char* front[],char* cmd[],int sockfd,int cmd_number){
	int tag,from,rfrom,childpid,me,fd[2],fe[2];
	me=findme(sockfd);
	tag=returnX(cmd,cmd_number);
	from=atoi(&cmd[tag][1]);
	rfrom=from-1;
	if(clitable[rfrom].pipeto[me]==1){
		broad_cast(7,sockfd,rfrom,line);
		clitable[rfrom].pipeto[me]=0;
		cmd_number=splitbyX(front,cmd,tag,cmd_number);
		if(cmd[0]==0){
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(clitable[rfrom].pipefd[me],0);
				dup2(sockfd,1);
				dup2(sockfd,2);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(clitable[rfrom].pipefd[me]);
			freecmd(front,tag+1);
		}
		else if(cmd[0][0]=='>'){
			int to,rto;
			to=atoi(&cmd[0][1]);
			rto=to-1;
			clitable[me].pipeto[rto]=1;
			broad_cast(6,sockfd,rto,line);
			pipe(fd);
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(clitable[rfrom].pipefd[me],0);
				dup2(fd[1],1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(fd[1]);
			close(clitable[rfrom].pipefd[me]);
			clitable[me].pipefd[rto]=fd[0];
			freecmd(front,tag+1);
		}
		else if(cmd[0][0]=='|'){
			int pipeN;
			pipe(fe);
			if(cmd[0][1]==0) pipeN=1;
			else	pipeN=atoi(&cmd[0][1]);
			createpipe(pipeN,fe[0],fe[1],sockfd);
			for(int i=0;i<cmd_number;i++){
				free(cmd[i]);
				if(i+1<cmd_number)	cmd[i]=strdup(cmd[i+1]);
				else cmd[i]=0;
			}
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				close(fe[0]);
				dup2(clitable[rfrom].pipefd[me],0);
				dup2(fe[1],1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(fe[1]);
			close(clitable[rfrom].pipefd[me]);
			freecmd(front,tag+1);
			return cmd_number-1;
		}
	}
	else{
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: the pipe #%d->#%d does not exist yet. ***\n",from,me+1);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	freecmd(cmd,cmd_number+1);
	return 0;
}

int pipe_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int fd[2],pipetag,childpid,status=0,pipeN=0;
	pipe(fd);
	pipetag=returnX(cmd,cmd_number);
	if(cmd[pipetag][1]==0) pipeN=1;
	else	pipeN=atoi(&cmd[pipetag][1]);
	createpipe(pipeN,fd[0],fd[1],sockfd);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		dup2(sockfd,2);
		execvpplus(sockfd,front);
	}
	else wait(&status);
	close(fd[1]);
	freecmd(front,pipetag+1);
	if(WEXITSTATUS(status)==1){
		freecmd(cmd,cmd_number+1);
		cmd_number=0;
		pluspipe(sockfd);
	}
	return cmd_number;
}

int write_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int pipetag,childpid,fd[2];
	pipetag=returnX(cmd,cmd_number);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	FILE *file;
	file=fopen(cmd[0],"w");
	pipe(fd);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		execvpplus(sockfd,front);
	}
	else wait(NULL);
	close(fd[1]);
	char *buff=malloc((BUFFSIZE+1)*sizeof(char));
	int len;
	while((len=read(fd[0],buff,BUFFSIZE))){
	buff[len]=0;
	fprintf(file,"%s",buff);
	}
	close(fd[0]);
	fclose(file);
	free(buff);
	freecmd(front,pipetag+1);
	freecmd(cmd,cmd_number+1);
	cmd_number=0;
	return cmd_number;
}

//
// MAIN AREA
//

int main(int argc,char *argv[]){
	int	listener, newsockfd,fdmax;
	struct sockaddr_in	cli_addr, serv_addr;
	socklen_t clilen = sizeof(cli_addr);
	char line[MAXLINE]={0};
	FD_ZERO(&master);
	FD_ZERO(&readfd);
	char *welcome = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";
	if((listener = socket(AF_INET, SOCK_STREAM, 0))<0)
		err_dump("server: can't open stream socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_TCP_PORT);

	int opt=1;
	setsockopt(listener, SOL_SOCKET, SO_REUSEADDR,&opt, sizeof(opt));
	if(bind(listener, (struct sockaddr *) &serv_addr, sizeof(serv_addr))< 0)
		err_dump("server: can't bind local address");
	listen(listener, 5);
	FD_SET(listener,&master);
	fdmax=listener;
	initenv();
	for(;;){
		readfd=master;
		if(select(fdmax+1,&readfd,0,0,0)<0)
			err_dump("select error");
		for(int i=0;i<fdmax+1;i++){
			if(FD_ISSET(i,&readfd)){
				if(i==listener){
					clilen=sizeof(cli_addr);
					newsockfd = accept(listener, (struct sockaddr *) &cli_addr, &clilen);
					if(newsockfd==-1){
						err_dump("accept error");
					}
					else{
						FD_SET(newsockfd,&master);
						write(newsockfd,welcome,strlen(welcome));
						create_user(cli_addr,newsockfd);
						profix(newsockfd);
						if(newsockfd>fdmax)	fdmax=newsockfd;
					}
				}
				else{	// i!=listener
					if(readline(i,line,MAXLINE)<=0){
						err_dump("readfile error");
					}
					else{
						int me=findme(i);
						setenv("PATH",clitable[me].path,1);
						char *cmd[MAX_CMDNUMBER];
						char *front[MAX_CMDNUMBER];
						int tag,childpid,cmd_number,status=0;
						cmd_number=cutline(line, cmd," ");
						while(cmd_number){
							tableclock(i);
							tag=classify_X(cmd,cmd_number);
							if (tag==3){
								if (strcmp(cmd[0],"yell")==0) yell_cmd(line,cmd_number,i);
								if (strcmp(cmd[0],"tell")==0) tell_cmd(cmd,line,cmd_number,i);
								freecmd(cmd,cmd_number+1);
								cmd_number=0;
							}
							else if (tag==4) cmd_number=sendpipe_cmd(line,front,cmd,i,cmd_number);
							else if (tag==5) cmd_number=readpipe_cmd(line,front,cmd,i,cmd_number);
							else if (tag==1) cmd_number=pipe_cmd(front,cmd,i,cmd_number);
							else if (tag==2) cmd_number=write_cmd(front,cmd,i,cmd_number);
							else{
								if (strcmp(cmd[0],"exit")==0) exit_cmd(i);
								else if (strcmp(cmd[0],"setenv")==0) 	setenv_cmd(i,cmd);
								else if (strcmp(cmd[0],"printenv")==0) printenv(i,cmd);
								else if (strcmp(cmd[0],"who")==0) who_cmd(i);
								else if (strcmp(cmd[0],"name")==0) name_cmd(cmd,i);
								else{
									if((childpid = fork())<0) err_dump("server:fork error");
									else if(childpid == 0){
										dup2(i,1);
										dup2(i,2);
										execvpplus(i,cmd);
									}
									else wait(&status);
								}
								freecmd(cmd,cmd_number+1);
								cmd_number=0;
							}
							closepipe(i);
						} //while
						profix(i);
					}
				}
			}
		}
	}
}
