#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/shm.h>
#define PERM 0666
#define MAXLINE 10000
#define SERV_TCP_PORT 10110
#define MAX_CMDNUMBER 4000
#define PATHSIZE 1024
#define BUFFSIZE 8192

int pipetable[1000][2]={{0}};
int shmid_user,shmid_event;
typedef struct usertable{
	int valid;
	int id;
	int pid;
	char name[21];
	char ip_port[22];
	int pipeto[30];
	int pipefd[30];
	char buff[10][1024];
}Usertable;
typedef struct broadcast{
	int flag;
	int from;
	int to;
	char message[1024];
}Broadcast;
Usertable *shm_user;
Broadcast *shm_event;

int is_me(){
	pid_t pid=getpid();
	for(int i=0;i<30;i++){
		if(shm_user[i].pid==pid) return i;
	}
	return -1;
}

void sig_handle(int sig){
	int stat,me=is_me();
	char *buff=malloc(sizeof(char)*BUFFSIZE);
	switch(sig){
		case 2:	//ctrl+c
		shmdt(shm_user);
		shmctl(shmid_user,IPC_RMID,0);
		shmdt(shm_event);
		shmctl(shmid_event,IPC_RMID,0);
		printf("\n");
		exit(0);
		break;
		case 10:	// kill
		if(shm_event->flag==2){
			int ex=shm_event->from;
			if(shm_user[me].pipeto[ex]==1){
				shm_user[me].pipeto[ex]=0;
				char *tmppipe=malloc(sizeof(char)*100);
				sprintf(tmppipe,"/tmp/pi544pe%dto%d",me,ex);
				unlink(tmppipe);
			}
		}
		if(shm_event->flag==5){
			sprintf(buff,"*** %s told you ***: %s\n",shm_user[shm_event->from].name,shm_event->message);
			write(4,buff,strlen(buff));
		}
		if(shm_event->flag==7){
			int t=shm_event->from;
			int x=shm_event->to;
			if(x==me) close(shm_user[x].pipefd[t]);
		}
		for(int i=0;i<10;i++){
			if(strlen(shm_user[me].buff[i])!=0){
				write(4,shm_user[me].buff[i],strlen(shm_user[me].buff[i]));
				memset(shm_user[me].buff[i],0,1024);
			}
		}
		break;
		case 17:	//zombie
		waitpid(0,&stat,WNOHANG);
		break;
		default:
		break;
	}
	free(buff);
}

// initial env
void initenv(){
	char path[PATHSIZE];
	getcwd(path,PATHSIZE);
	strcat(path,"/ras");
	chdir(path);
	chroot(path);
	setenv("PATH","bin:.",1);
}

void profix(int sockfd){
	char *pro = "% ";
	write(sockfd,pro,2);
}

int err_dump(char* p){
	perror(p);
	exit(1);
}

void send_signal(int flag,int from){
	char *buff=malloc(sizeof(char)*1024);
	shm_event->flag=flag;
	if(shm_event->flag==1){
		sprintf(buff,"*** User '(no name)' entered from %s. ***\n",shm_event->message);
	}
	if(shm_event->flag==2){
		sprintf(buff,"*** User '%s' left. ***\n",shm_event->message);
	}
	if(shm_event->flag==3){
		int t=shm_event->from;
		sprintf(buff,"*** User from %s is named '%s'. ***\n",shm_user[t].ip_port,shm_user[t].name);
	}
	if(shm_event->flag==4){
		sprintf(buff,"*** %s yelled ***: %s\n",shm_user[shm_event->from].name,shm_event->message);
	}
	if(shm_event->flag==6){
		int t=shm_event->from;
		int x=shm_event->to;
		sprintf(buff,"*** %s (#%d) just piped '%s' to %s (#%d) ***\n",shm_user[t].name,t+1,\
						shm_event->message,shm_user[x].name,x+1);
	}
	if(shm_event->flag==7){
		int t=shm_event->from;
		int x=shm_event->to;
		sprintf(buff,"*** %s (#%d) just received from %s (#%d) by '%s' ***\n",shm_user[t].name,t+1,\
						shm_user[x].name,x+1,shm_event->message);
	}
	for(int i=0;i<30;i++){
		if(i==from) continue;
		else if(shm_user[i].valid==1){
			for(int j=0;j<10;j++){
				if(strlen(shm_user[i].buff[j])==0){
					strcpy(shm_user[i].buff[j],buff);
					break;
				}
			}
			kill(shm_user[i].pid,10);
		}
	}
	free(buff);
}

void init_user(){
	for(int i=0;i<30;i++){
		shm_user[i].id=i+1;
		shm_user[i].valid=0;
		shm_user[i].pid=0;
		for(int j=0;j<30;j++){
			shm_user[i].pipeto[j]=0;
			shm_user[i].pipefd[j]=0;
		}
		for(int j=0;j<10;j++){
			memset(shm_user[i].buff[j],0,1024);
		}
		memset(shm_user[i].ip_port,0,22);
		strcpy(shm_user[i].name,"(no name)");
	}
}

int create_user(struct sockaddr_in cli_addr){
	// char name[20]={0};
	int here=0;
	for(int i=0;i<30;i++){
		if(shm_user[i].valid==0){
			shm_user[i].valid=1;
			here=i;
			// inet_ntop(AF_INET,&cli_addr.sin_addr,shm_user[i].ip_port,16);
			// strcat(shm_user[i].ip_port,"/");
			// sprintf(name,"%u",cli_addr.sin_port);
			// strcat(shm_user[i].ip_port,name);
			// strcpy(shm_event->message,shm_user[i].ip_port);
			strcpy(shm_user[i].ip_port,"CGILAB/511");
			strcpy(shm_event->message,"CGILAB/511");
			break;
		}
	}
	send_signal(1,here);
	return here;
}

void delete_user(){
	int i=is_me();
	shm_user[i].valid=0;
	shm_user[i].pid=0;
	memset(shm_user[i].ip_port,0,22);
	strcpy(shm_event->message,shm_user[i].name);
	strcpy(shm_user[i].name,"(no name)");
	shm_event->from=i;
	send_signal(2,i);
	char* buff=malloc(BUFFSIZE*sizeof(char));
	sprintf(buff,"*** User '%s' left. ***\n",shm_event->message);
	write(4,buff,strlen(buff));
	free(buff);
}

//	readline also cut blank

int readline(int fd,char *ptr, int maxlen){
	int n, rc;
	char c;
	for(n=1;n<maxlen; n++){
		if ((rc=read(fd, &c, 1))==1){
			if (c == '\r')
				continue;
			if (c == '\n')
				break;
			*ptr++ = c;
		}else if(rc==0){
			if(n==1) return(0);
			else break;
		}else
			return(-1);
	}
	*ptr = '\0';
	return(n);
}

// cut the line

int cutline(char *line, char *cmd[], char *cut){
	if(!line) return 0;
	char *str = NULL;
	char *ptr = NULL;
	char *dup = strdup(line);

	str = strtok_r(dup, cut, &ptr);
	if (str == NULL){
		err_dump("error : client no input\n");
		free(dup);
		return -1;
	}
	int count =0;
	do{
		cmd[count]=strdup(str);
		str = strtok_r(NULL, cut, &ptr);
		count++;
	}while(str);
	cmd[count]=NULL;
	free(dup);
	return count;
}
// environment configure

void printenv(int sockfd,char *cmd[]){
	if (cmd[1] != NULL){
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		sprintf(text,"%s=%s\n",cmd[1],getenv(cmd[1]));
		write(sockfd,text,strlen(text));
		free(text);
	}
}

void setenv_cmd(int sockfd, char *cmd[]){
	if (cmd[2]!=NULL){
		setenv(cmd[1],cmd[2],1);
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		// buy DLC to unlock
		//sprintf(text,"setenv %s=%s\n",cmd[1],cmd[2]);
		//write(sockfd,text,strlen(text));
		free(text);
	}
}

void exit_cmd(){
	delete_user();
	exit(0);
}

void who_cmd(int sockfd){
	char *buff;
	buff=malloc(100*sizeof(char));
	int t=is_me();
	sprintf(buff,"<ID>\t<nickname>\t<IP/port>\t<indicate me>\n");
	write(sockfd,buff,strlen(buff));
	for (int i=0;i<30;i++){
		if(shm_user[i].valid==1){
			sprintf(buff,"%d\t%s\t%s",shm_user[i].id,shm_user[i].name,shm_user[i].ip_port);
			if(i==t) strcat(buff,"\t<-me\n");
			else strcat(buff,"\n");
			write(sockfd,buff,strlen(buff));
		}
	}
	free(buff);
}

void name_cmd(char *cmd[]){
	char *str=malloc(BUFFSIZE*sizeof(char));
	for(int i=0;i<30;i++){
		if(strcmp(shm_user[i].name,cmd[1])==0){
			sprintf(str,"*** User '%s' already exists. ***\n",cmd[1]);
			write(4,str,strlen(str));
			free(str);
			return;
		}
	}
	int j=is_me();
	strcpy(shm_user[j].name,cmd[1]);
	shm_event->from=j;
	free(str);
	send_signal(3,-1);
}

void yell_cmd(char *line,int number){
	char *ptr = NULL;
	char *dup = strdup(line);
	strtok_r(dup," ", &ptr);
	strcpy(shm_event->message,ptr);
	shm_event->from=is_me();
	send_signal(4,-1);
	free(dup);
}

void tell_cmd(char *cmd[],char *line,int number){
	char *ptr = NULL;
	char *dup = strdup(line);
	strtok_r(dup," ", &ptr);
	strtok_r(NULL," ", &ptr);
	int tellto=atoi(cmd[1]),rtell=tellto-1;
	if(shm_user[rtell].valid==1){
		strcpy(shm_event->message,ptr);
		shm_event->flag=5;
		shm_event->from=is_me();
		kill(shm_user[rtell].pid,10);
	}
	else{
		char *buff=malloc(BUFFSIZE*sizeof(char));
		sprintf(buff,"*** Error: user #%d does not exist yet. ***\n",tellto);
		write(4,buff,strlen(buff));
		free(buff);
	}
	free(dup);
}

void tableclock(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]>0){
			if(pipetable[i][0]==1){
				pipetable[i][0]=-1;
				dup2(pipetable[i][1],0);
			}
			else pipetable[i][0]--;
		}
	}
}

void pluspipe(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]>0){
				pipetable[i][0]++;
			}
			else if(pipetable[i][0]==-1) pipetable[i][0]=1;
		}
}

void closepipe(){
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]==-1){
			pipetable[i][0]=0;
			close(pipetable[i][1]);
		}
	}
}

void createpipe(int pn,int fd0,int fd1){
	int insert=-1;
	for(int i=0;i<1000;i++){
		if(pipetable[i][0]==0){
			pipetable[i][1]=fd0;
			pipetable[i][0]=pn;
			insert = i;
			break;
		}
	}
	if(insert==-1) err_dump("pipetable full");
	int len=0;
	char *buff;
	buff=malloc(BUFFSIZE*sizeof(char));
	for(int i=0;i<1000;i++){
		if(i==insert) continue;
		else if(pipetable[i][0]==pn){
			while((len=read(pipetable[i][1],buff,BUFFSIZE))){
			write(fd1,buff,len);
			}
			close(pipetable[i][1]);
			pipetable[i][0]=0;
			break;
		}
	}
	free(buff);
}

// check pipe

int classify_X(char *cmd[],int count){
	if(strcmp(cmd[0],"yell")==0) return 3;
	if(strcmp(cmd[0],"tell")==0) return 3;
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return 1;
		if (cmd[i][0]=='<') return 5;
		if (cmd[i][0]=='>'&&cmd[i][1]!=0) return 4;
		if (cmd[i][0]=='>') return 2;
	}
	return 0;
}

int returnX(char *cmd[],int count){
	for(int i=0;i<count;i++){
		if (cmd[i][0]=='|') return i;
		if (cmd[i][0]=='>') return i;
		if (cmd[i][0]=='<') return i;
	}
	return 0;
}

int splitbyX(char* front[],char* cmd[],int pn,int cn){
  char* t[MAX_CMDNUMBER];
  for(int i=0;i<cn;i++){
    t[i]=strdup(cmd[i]);
  }
  for(int i=0;i<cn+1;i++){
    free(cmd[i]);
  }
  for(int i=0;i<pn;i++){
    front[i]=strdup(t[i]);
  }
  front[pn]=0;
  for(int i=pn+1;i<cn;i++){
    cmd[i-pn-1]=strdup(t[i]);
  }
  cmd[cn-pn-1]=0;
  for(int i=0;i<cn;i++){
    free(t[i]);
  }
  return cn-pn-1;
}

void execvpplus(int sockfd,char *cmd[]){
	if(execvp(cmd[0],cmd)==-1){
		char *text;
		text = malloc((sizeof(char))*(BUFFSIZE));
		sprintf(text,"Unknown command: [%s].\n",cmd[0]);
		write(sockfd,text,strlen(text));
		free(text);
		exit(1);
	}
	exit(0);
}

void freecmd(char *cmd[],int count){
	for(int i=0;i<count;i++)
	free(cmd[i]);
}

int sendpipe_cmd(char *line,char* front[],char* cmd[],int sockfd,int cmd_number){
	int tag,to,phyto,childpid,rd,wr,me;
	char *tmppipe=malloc(sizeof(char)*100);
	me=is_me();
	tag=returnX(cmd,cmd_number);
	to=atoi(&cmd[tag][1]);
	phyto=to-1;
	if (shm_user[phyto].valid==0){
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: user #%d does not exist yet. ***\n",to);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	else if(shm_user[me].pipeto[phyto]==0){
		shm_user[me].pipeto[phyto]=1;
		cmd_number=splitbyX(front,cmd,tag,cmd_number);
		sprintf(tmppipe,"/tmp/pi544pe%dto%d",me,phyto);
		if(mkfifo(tmppipe,PERM)<0)
		err_dump("mkfifo fail");
		rd=open(tmppipe,O_RDONLY|O_NONBLOCK);
		wr=open(tmppipe,O_WRONLY);
		if(cmd[0]!=0&&cmd[0][0]=='<'){
			int from,rfrom,frd;
			from=atoi(&cmd[0][1]);
			rfrom=from-1;
			shm_user[rfrom].pipeto[me]=0;
			sprintf(tmppipe,"/tmp/pi544pe%dto%d",rfrom,me);
			frd=open(tmppipe,O_RDONLY|O_NONBLOCK);
			unlink(tmppipe);
			strcpy(shm_event->message,line);
			shm_event->from=me;
			shm_event->to=rfrom;
			send_signal(7,-1);
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(frd,0);
				dup2(wr,1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(frd);
		}
		else{
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(wr,1);
				execvpplus(sockfd,front);
			}
			else wait(0);
		}
		shm_user[me].pipefd[phyto]=rd;
		shm_event->from=me;
		shm_event->to=phyto;
		strcpy(shm_event->message,line);
		send_signal(6,-1);
		close(wr);
		freecmd(front,tag+1);
	}
	else{
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: the pipe #%d->#%d already exists. ***\n",me+1,to);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	freecmd(cmd,cmd_number+1);
	free(tmppipe);
	return 0;
}

int readpipe_cmd(char *line,char* front[],char* cmd[],int sockfd,int cmd_number){
	int tag,from,phyfrom,childpid,rd,me;
	char *tmppipe=malloc(sizeof(char)*100);
	me=is_me();
	tag=returnX(cmd,cmd_number);
	from=atoi(&cmd[tag][1]);
	phyfrom=from-1;
	if(shm_user[phyfrom].pipeto[me]==1){
		shm_user[phyfrom].pipeto[me]=0;
		sprintf(tmppipe,"/tmp/pi544pe%dto%d",phyfrom,me);
		rd=open(tmppipe,O_RDONLY|O_NONBLOCK);
		unlink(tmppipe);
		cmd_number=splitbyX(front,cmd,tag,cmd_number);
		strcpy(shm_event->message,line);
		shm_event->from=me;
		shm_event->to=phyfrom;
		send_signal(7,-1);
		if(cmd[0]==0){
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(rd,0);
				dup2(sockfd,1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(rd);
			freecmd(front,tag+1);
		}
		else if(cmd[0][0]=='>'){
			int to,rto,wr,frd;
			to=atoi(&cmd[0][1]);
			rto=to-1;
			shm_user[me].pipeto[rto]=1;
			sprintf(tmppipe,"/tmp/pi544pe%dto%d",me,rto);
			if(mkfifo(tmppipe,PERM)<0)
				err_dump("mkfifo fail");
			frd=open(tmppipe,O_RDONLY|O_NONBLOCK);
			wr=open(tmppipe,O_WRONLY);
			shm_user[me].pipefd[rto]=frd;
			shm_event->from=me;
			shm_event->to=rto;
			strcpy(shm_event->message,line);
			send_signal(6,-1);
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				dup2(rd,0);
				dup2(wr,1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(wr);
			close(rd);
			freecmd(front,tag+1);
		}
		else if(cmd[0][0]=='|'){
			int fd[2],pipeN;
			pipe(fd);
			if(cmd[0][1]==0) pipeN=1;
			else	pipeN=atoi(&cmd[0][1]);
			createpipe(pipeN,fd[0],fd[1]);
			for(int i=0;i<cmd_number;i++){
				free(cmd[i]);
				if(i+1<cmd_number)	cmd[i]=strdup(cmd[i+1]);
				else cmd[i]=0;
			}
			if((childpid = fork())<0) err_dump("server:fork error");
			else if(childpid == 0){
				close(fd[0]);
				dup2(rd,0);
				dup2(fd[1],1);
				execvpplus(sockfd,front);
			}
			else wait(0);
			close(fd[1]);
			close(rd);
			freecmd(front,tag+1);
			free(tmppipe);
			return cmd_number-1;
		}
		else err_dump("fuck you");
	}
	else{
		char* msg=malloc(BUFFSIZE*sizeof(char));
		sprintf(msg,"*** Error: the pipe #%d->#%d does not exist yet. ***\n",from,me+1);
		write(sockfd,msg,strlen(msg));
		free(msg);
	}
	freecmd(cmd,cmd_number+1);
	free(tmppipe);
	return 0;
}

int pipe_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int fd[2],pipetag,childpid,status=0,pipeN=0;
	pipe(fd);
	pipetag=returnX(cmd,cmd_number);
	if(cmd[pipetag][1]==0) pipeN=1;
	else	pipeN=atoi(&cmd[pipetag][1]);
	createpipe(pipeN,fd[0],fd[1]);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		execvpplus(sockfd,front);
	}
	else wait(&status);
	close(fd[1]);
	freecmd(front,pipetag+1);
	if(WEXITSTATUS(status)==1){
		freecmd(cmd,cmd_number+1);
		cmd_number=0;
		pluspipe();
	}
	return cmd_number;
}

int write_cmd(char *front[],char *cmd[],int sockfd,int cmd_number){
	int pipetag,childpid,fd[2];
	pipetag=returnX(cmd,cmd_number);
	cmd_number=splitbyX(front,cmd,pipetag,cmd_number);
	FILE *file;
	file=fopen(cmd[0],"w");
	pipe(fd);
	if((childpid = fork())<0) err_dump("server:fork error");
	else if(childpid == 0){
		close(fd[0]);
		dup2(fd[1],1);
		execvpplus(sockfd,front);
	}
	else wait(NULL);
	close(fd[1]);
	char *buff=malloc((BUFFSIZE+1)*sizeof(char));
	int len;
	while((len=read(fd[0],buff,BUFFSIZE))){
	buff[len]=0;
	fprintf(file,"%s",buff);
	}
	close(fd[0]);
	fclose(file);
	free(buff);
	freecmd(front,pipetag+1);
	freecmd(cmd,cmd_number+1);
	cmd_number=0;
	return cmd_number;
}

// str_back

int str_back(int sockfd){
	int num;
	char *line;
	char *cmd[MAX_CMDNUMBER];
	char *front[MAX_CMDNUMBER];
	int tag;
	int childpid;
	int cmd_number;
	int status=0;
	dup2(sockfd,2);
	signal(SIGUSR1,sig_handle);
	for(;;){
		line=malloc(MAXLINE*sizeof(char));
		profix(sockfd);
		num = readline(sockfd, line, MAXLINE);
		if(num==0) return 0;
		else if (num<0) err_dump("str_echo:readline error");
		// cut the line
		cmd_number=cutline(line, cmd," ");
		while(cmd_number){
			tableclock();
			tag=classify_X(cmd,cmd_number);
			if (tag==3){
				if (strcmp(cmd[0],"yell")==0) yell_cmd(line,cmd_number);
				if (strcmp(cmd[0],"tell")==0) tell_cmd(cmd,line,cmd_number);
				freecmd(cmd,cmd_number+1);
				cmd_number=0;
			}
			else if (tag==1) cmd_number=pipe_cmd(front,cmd,sockfd,cmd_number);
			else if (tag==4) cmd_number=sendpipe_cmd(line,front,cmd,sockfd,cmd_number);
			else if (tag==5) cmd_number=readpipe_cmd(line,front,cmd,sockfd,cmd_number);
			else if (tag==2) cmd_number=write_cmd(front,cmd,sockfd,cmd_number);
			else{
				if (strcmp(cmd[0],"exit")==0) exit_cmd();
				else if (strcmp(cmd[0],"setenv")==0) 	setenv_cmd(sockfd,cmd);
				else if (strcmp(cmd[0],"printenv")==0) printenv(sockfd,cmd);
				else if (strcmp(cmd[0],"who")==0) who_cmd(sockfd);
				else if (strcmp(cmd[0],"name")==0) name_cmd(cmd);
				else{
					if((childpid = fork())<0) err_dump("server:fork error");
					else if(childpid == 0){
						dup2(sockfd,1);
						execvpplus(sockfd,cmd);
					}
					else wait(&status);
				}
				freecmd(cmd,cmd_number+1);
				cmd_number=0;
			}
			closepipe();
		} //while
		free(line);
	} //for
}	//str_back

//
// MAIN AREA
//

int main(int argc,char *argv[]){
	int	sockfd, newsockfd, childpid,here;
	struct sockaddr_in	cli_addr, serv_addr;
	key_t key = 24601;
	char *welcome = "****************************************\n\
** Welcome to the information server. **\n\
****************************************\n";
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0))<0)
		err_dump("server: can't open stream socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_TCP_PORT);

	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&opt, sizeof(opt));
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))< 0)
		err_dump("server: can't bind local address");
	listen(sockfd, 5);

	shmid_user=shmget(key,30*sizeof(Usertable),PERM|IPC_CREAT);
	shm_user=shmat(shmid_user,0,0);
	shmid_event=shmget(key+100,sizeof(Broadcast),PERM|IPC_CREAT);
	shm_event=shmat(shmid_event,0,0);

	initenv();
	init_user();
	signal(SIGINT,sig_handle);
	signal(SIGCHLD,sig_handle);
	for(;;){
		socklen_t clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		here=create_user(cli_addr);
		if(newsockfd < 0) err_dump("server: accept error");
		if((childpid = fork())<0) err_dump("server:fork error");
		else if(childpid == 0){
			close(sockfd);
			shm_user[here].pid=getpid();
			// welcome
			write(newsockfd,welcome,strlen(welcome));
			char* buff=malloc(BUFFSIZE*sizeof(char));
			sprintf(buff,"*** User '(no name)' entered from %s. ***\n",shm_user[here].ip_port);
			write(4,buff,strlen(buff));
			free(buff);
			str_back(newsockfd);
			exit(0);
		}else{
			close(newsockfd);
		}
	}
}
